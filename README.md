# OpenML dataset: AI4I2020

https://www.openml.org/d/42890

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.csv`](./dataset/tables/data.csv): CSV file with data
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

The AI4I 2020 Predictive Maintenance Dataset is a synthetic dataset that reflects real predictive maintenance data encountered in industry. Since real predictive maintenance datasets are generally difficult to obtain and in particular difficult to publish, we present and provide a synthetic dataset that reflects real predictive maintenance encountered in industry to the best of our knowledge.

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/42890) of an [OpenML dataset](https://www.openml.org/d/42890). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/42890/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/42890/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/42890/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

